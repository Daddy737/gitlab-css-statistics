const { URL } = require('url');
const { mkdirSync, existsSync, writeFileSync } = require('fs');

const puppeteer = require('puppeteer');
const config = require('../crawler_config');

console.log('URLS : ' + config.pages.length);

if (!existsSync('tmp')) {
  mkdirSync('tmp');
}

if (!existsSync('tmp/crawled_pages')) {
  mkdirSync('tmp/crawled_pages');
}

(async () => {
  // Wait for browser launching.
  const browser = await puppeteer.launch();
  // Wait for creating the new page.
  const page = await browser.newPage();

  // await this.crawlInternal(page, `${this.baseUrl}/index.html`, site['children'], site['name']);

  for (let i = 0; i < config.pages.length; i++) {
    const path = config.pages[i];
    try {
      // Go to the target page.
      let url = new URL(path);
      const newFileName = url.pathname.replace(/\//gi, '_');
      console.log('Crawling page ', path);
      await page.goto(path, { waitUntil: 'networkidle2' });

      const result = await page.evaluate(() => {
        const scriptTags = window.document.querySelectorAll('script');
        scriptTags.forEach((node) => {
          node.parentNode.removeChild(node);
        });

        const cssTags = window.document.querySelectorAll(
          'link[rel="stylesheet"]'
        );
        cssTags.forEach((node) => {
          node.parentNode.removeChild(node);
        });
      });

      const html = await page.content();
      writeFileSync(`tmp/crawled_pages/${newFileName}.html`, html);
    } catch (ex) {
      console.error('Problem crawling: ' + path + ':', ex);
    }
  }

  console.log('Closing browser');
  await browser.close();
})();
