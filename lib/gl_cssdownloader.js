const http = require('http');
const https = require('https');
const { mkdirSync, existsSync, createWriteStream } = require('fs');
const puppeteer = require('puppeteer');

const config = require('../crawler_config');

if (!existsSync('tmp')) {
  mkdirSync('tmp');
}

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const downloadCSS = async (baseUrl, savePath) => {
    try {
      // Go to the target page.
      console.log('Opening page ', baseUrl);
      await page.goto(baseUrl, { waitUntil: 'networkidle2' });

      const cssUrl = await page.evaluate(() => {
        const cssTags = window.document.querySelectorAll(
          'link[rel="stylesheet"]'
        );
        let cssUrl;
        cssTags.forEach((node) => {
          console.log('Found Stylesheet with - ' + node.href);
          if (node.href.indexOf('application-') > -1) {
            console.log('Found CSS Reference: ' + node.href);
            cssUrl = node.href;
          } else if (node.href.indexOf('application_dark-') > -1) {
            console.log('Found CSS Dark Reference: ' + node.href);
            cssUrl = node.href;
          }
        });
        if (!cssUrl) cssUrl = cssTags.length;
        return cssUrl;
      });

      const file = createWriteStream(savePath);
      if (cssUrl) {
        if (cssUrl.indexOf('https://') > -1) {
          https.get(cssUrl, (res) => {
            res.pipe(file);
          });
        } else {
          http.get(cssUrl, (res) => {
            res.pipe(file);
          });
        }

        console.log('Result URL : ' + cssUrl);
      } else {
        console.error('No CSS Url was found for ' + baseUrl);
      }
    } catch (ex) {
      console.error(
        'Problem crawling CSS Base URL: ' + config.comBaseURL + ':',
        ex
      );
    }
  };

  await downloadCSS(config.comBaseURL, 'tmp/application-com.css');

  // Lets login locally into GDK
  await page.goto('http://127.0.0.1:3000/users/sign_in');

  // Login
  await page.type('#user_login', 'root');
  await page.type('#user_password', 'kritzendorf');
  await page.click('.btn.btn-confirm');
  await page.waitForNavigation();

  // Switch to standard
  await page.goto('http://127.0.0.1:3000/-/profile/preferences');
  await page.waitForTimeout(2000);
  await page.click('#user_theme_id_1');
  await page.waitForTimeout(2000);

  await downloadCSS(config.localBaseURL, 'static/application-local.css');

  // Switch to dark
  await page.goto('http://127.0.0.1:3000/-/profile/preferences');
  await page.click('#user_theme_id_11');
  page.waitForTimeout(2000);

  await downloadCSS(config.localBaseURL, 'tmp/application-dark.css');

  await page.goto('http://127.0.0.1:3000/-/profile/preferences');
  await page.waitForTimeout(2000);
  await page.click('#user_theme_id_1');

  console.log('Closing browser');
  await browser.close();
})();
