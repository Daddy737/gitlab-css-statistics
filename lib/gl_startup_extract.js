const { PurgeCSS } = require('purgecss');
const postcss = require('postcss');
const puppeteer = require('puppeteer');
const csso = require('csso');

const { URL } = require('url');
const {
  mkdirSync,
  existsSync,
  writeFileSync,
  readFileSync,
  statSync,
} = require('fs');

const config = require('../crawler_config');

if (!existsSync('tmp')) {
  mkdirSync('tmp');
}

if (!existsSync('tmp/crawled_startup_pages')) {
  mkdirSync('tmp/crawled_startup_pages');
}

if (!existsSync('tmp/startup_css')) {
  mkdirSync('tmp/startup_css');
}

(async () => {
  const additionalCss = await readFileSync('startup-addon.css');
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.goto('http://127.0.0.1:3000/users/sign_in');

  // Login
  await page.type('#user_login', 'root');
  await page.type('#user_password', 'kritzendorf');
  await page.click('.btn.btn-confirm');
  await page.waitForNavigation();

  for (let i = 0; i < config.startupCSS.pages.length; i++) {
    try {
      const newFileName = config.startupCSS.pages[i].name;

      await page.goto('http://127.0.0.1:3000/profile/preferences');
      if (newFileName.indexOf('dark') > -1) {
        // Lets switch to dark mode
        await page.click('#user_theme_id_11');
      } else {
        // Lets switch to default mode
        await page.click('#user_theme_id_1');
      }
      // Let the system save our theme
      page.waitFor(2000);

      await page.goto(config.startupCSS.pages[i].url, {
        waitUntil: 'networkidle2',
      });

      await page.evaluate((removeElements) => {
        const scriptTags = window.document.querySelectorAll('script');
        scriptTags.forEach((node) => {
          node.parentNode.removeChild(node);
        });

        const cssTags = window.document.querySelectorAll(
          'link[rel="stylesheet"]'
        );
        cssTags.forEach((node) => {
          node.parentNode.removeChild(node);
        });

        // Remove elements we don't want to have in the CSS
        for (let j = 0; j < removeElements.length; j++) {
          if (window.document.querySelectorAll(removeElements[j])) {
            const list = window.document.querySelectorAll(removeElements[j]);
            for (let nodeItem of list) {
              nodeItem.remove();
            }
          } else {
            console.log(removeElements[j] + ' not found on page');
          }
        }
      }, config.startupCSS.removeDomElements);

      const html = await page.content();
      page.screenshot({ path: `tmp/crawled_startup_pages/${newFileName}.png` });
      writeFileSync(`tmp/crawled_startup_pages/${newFileName}.html`, html);

      const cssFileName = newFileName.indexOf('dark') > -1 ? 'dark' : 'local';

      const purgeCSSResult = await new PurgeCSS().purge({
        content: [`tmp/crawled_startup_pages/${newFileName}.html`],
        css: [`tmp/application-${cssFileName}.css`],
        fontFace: true,
        variables: true,
        keyframes: true,
      });

      const combinedCSS = purgeCSSResult[0].css + additionalCss;

      writeFileSync(
        `tmp/startup_css/startup-${config.startupCSS.pages[i].name}-raw.css`,
        combinedCSS
      );

      const cssRoot = postcss.parse(combinedCSS, {
        from: 'static/application-local.css',
      });

      const remNodes = [];

      const checkNodes = (nodes) => {
        for (let u = 0; u < nodes.length; u++) {
          if (nodes[u].type !== 'comment') {
            const node = nodes[u];

            // console.log('TYPE ' + nodes[u].type);
            if (node.params && node.name.indexOf('keyframes') > -1) {
              console.log('PARAMS ' + node.type);
            }

            if (
              node.type === 'atrule' &&
              (node.params === 'print' ||
                node.params === 'prefers-reduced-motion: reduce' ||
                node.name === 'keyframes')
            ) {
              console.log('Removing AtRule ' + node.params);
              remNodes.push(nodes[u]);
            }

            //Check for selector
            if (node.selector) {
              // Lets remove selectors with :focus as the startup render doesn't need it
              if (node.selectors) {
                const useSelectors = [];
                for (let x = 0; x < node.selectors.length; x++) {
                  if (
                    !(
                      node.selectors[x].indexOf(':hover') > -1 ||
                      node.selectors[x].indexOf(':focus') > -1 ||
                      node.selectors[x].indexOf('-webkit-') > -1 ||
                      node.selectors[x].indexOf('-moz-focusring-') > -1 ||
                      node.selectors[x].indexOf('-ms-expand') > -1
                    )
                  ) {
                    useSelectors.push(node.selectors[x]);
                  } else {
                    remNodes.push(nodes[u]);
                    console.warn(
                      'Remove node ' +
                        node.selector +
                        ' due to hover/focus only states'
                    );
                  }
                }
                if (useSelectors.length !== node.selectors.length) {
                  node.selectors = useSelectors;
                } else if (useSelectors.length === 0) {
                  remNodes.push(nodes[u]);
                  console.warn(
                    'Remove node ' +
                      node.selector +
                      ' due to hover/focus only states'
                  );
                }
              }

              if (
                config.startupCSS.enforceClassRemovalExact.indexOf(
                  node.selector
                ) > -1
              ) {
                remNodes.push(nodes[u]);
                console.warn(
                  'Removed node ' +
                    node.selector +
                    ' due to enforceClassRemoval setting'
                );
              }

              for (
                let v = 0;
                v < config.startupCSS.enforceClassRemovalWildcard.length;
                v++
              ) {
                if (
                  node.selector.indexOf(
                    config.startupCSS.enforceClassRemovalWildcard[v]
                  ) > -1
                ) {
                  remNodes.push(nodes[u]);
                  console.warn(
                    'Removed node ' +
                      node.selector +
                      ' due to enforceClassRemovalWildcard setting'
                  );
                }
              }
            }

            // Lets remove transition rules from the node
            if (
              node.type === 'decl' &&
              (node.prop === 'transition' ||
                node.prop.indexOf('-webkit-') > -1 ||
                node.prop.indexOf('-ms-') > -1)
            ) {
              remNodes.push(nodes[u]);
            }

            if (node.nodes && node.nodes.length > 0) {
              checkNodes(node.nodes);
            }
          } else {
            remNodes.push(nodes[u]);
          }
        }
      };

      checkNodes(cssRoot.nodes);

      remNodes.forEach((node) => {
        node.remove();
      });

      const remEmptyNodes = [];
      const checkForEmptyNodes = (nodes) => {
        // Lets check and remove all empty size queries
        for (let u = 0; u < nodes.length; u++) {
          const node = nodes[u];
          if (node.nodes && node.nodes.length === 0) {
            remEmptyNodes.push(node);
          } else if (node.nodes && node.nodes.length > 0) {
            checkForEmptyNodes(node.nodes);
          }
        }
      };

      checkForEmptyNodes(cssRoot.nodes);

      remEmptyNodes.forEach((node) => {
        node.remove();
      });

      const finalCSS = cssRoot.toResult().css;

      // Base output
      writeFileSync(
        `tmp/startup_css/startup-${config.startupCSS.pages[i].name}.css`,
        finalCSS
      );

      const minifiedCss = csso.minify(finalCSS).css;
      writeFileSync(
        `tmp/startup_css/startup-${config.startupCSS.pages[i].name}.min.css`,
        minifiedCss
      );

      const minFileStats = statSync(
        `tmp/startup_css/startup-${config.startupCSS.pages[i].name}.min.css`
      );
      console.log('Minified File Size: ' + minFileStats.size);

      // Save to GL directly to test faster
      writeFileSync(
        `../gdk/gitlab/app/assets/stylesheets/startup/startup-${config.startupCSS.pages[i].name}.scss`,
        finalCSS
      );
    } catch (ex) {
      console.error('Problem crawling: ' + newFileName + ':', ex);
    }
  }

  console.log('Closing browser');
  await browser.close();
})();
