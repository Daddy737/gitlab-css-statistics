const uncss = require('uncss');
const { writeFileSync, readdirSync } = require('fs');
const path = require('path');

const config = require('../crawler_config');
const cssMap = require('../tmp/css-map.json');

const directoryPath = path.join(__dirname, '../tmp/crawled_pages');

const filesList = readdirSync(directoryPath);
const fullPathFilesList = [];

for (let i = 0; i < filesList.length; i++) {
  fullPathFilesList.push(directoryPath + '/' + filesList[i]);
}

console.log('All files : ', fullPathFilesList);

// Download current application.css

const files = fullPathFilesList,
  options = {
    banner: false,
    ignore: [],
    stylesheets: ['../../static/application-local.css'],
    report: true,
  };

uncss(files, options, (error, output, report) => {
  if (error) return console.error(error);

  report.selectors.unused.forEach((rejectedSelector) => {
    if (cssMap.nodes[rejectedSelector]) {
      cssMap.nodes[rejectedSelector].rejectedUncss = true;
      console.log(rejectedSelector + ' is unused');
    }
  });

  writeFileSync('tmp/css-map.json', JSON.stringify(cssMap, null, 2));

  writeFileSync(`tmp/new_application_uncss.css`, output);
});
